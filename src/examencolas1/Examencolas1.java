/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examencolas1;

/**
 *
 * @author Mix
 */
public class Examencolas1 {
      private Nodo first;
 private Nodo last;
 public Examencolas1()
 {
 first=null;
 last=null;
 }
 public void insertar(Object dato)
 {
 Nodo i=new Nodo(dato);
 i.setNext(null);
 if(first==null & last==null)
 {
 first=i;
 last=i;
 }
 last.setNext(i);
 last=last.getNext();
 }
 public Object extraer()
 {
 Object dato=first.getDato();
 first=first.getNext();
 return dato;
 }
 public boolean estaVacia()
 {
 boolean cola=false;
 if(first==null & last==null)
 {
 cola=true;
 System.out.println("La cola esta vacia");
 }
 else
 {
 System.out.println("La cola no esta vacia");
 cola=false;
 }
 return cola;
 }
 public int contar()
 {
 int contador=0;
 Nodo c=this.first;
 while(c!=null)
 {
 contador++;
 c=c.getNext();
 }
 System.out.println("Numero de datos en la cola: "+contador);
 return contador;
 }
 public String toString()
 {
 Nodo c=this.first;
 String s="";
 while(c!=null)
 {
 s=s+c.toString();
 c=c.getNext();
 }
 return s;
 } 
}
