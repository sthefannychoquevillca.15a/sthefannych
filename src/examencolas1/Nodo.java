/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examencolas1;

/**
 *
 * @author Mix
 */
public class Nodo {
  private Object dato;
    private Nodo next;

    public Nodo(Object dato) {
        this.dato = dato;
    }

    public Object getDato() {
        return dato;
    }

    public void setDato(Object dato) {
        this.dato = dato;
    }

    public Nodo getNext() {
        return next;
    }

    public void setNext(Nodo next) {
        this.next = next;
    }
    @Override
    public String toString() {
        String s=" "+dato+" ";
        return s;
    }
}
